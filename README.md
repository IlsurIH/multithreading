# README #

This README for multithreading example

### How do I get set up? ###

* Clone repository
```
#!bash

cd path/to/repo
cd tagret
```
* for mac and linux use 
```
#!bash

chmod +x run.sh
./run.sh 7
```
* for windows use
```
#!bash
run.bat 7
```

### How to buld it###


```
#!java

mvn clean compile assembly:single
```