package ru.ilsurih.java;

public class Main {

    private final Monitor monitor = new Monitor();

    static String[] threads = new String[]{

            "Arr",
            "Bread",
            "Cheese",
            "Dinner",
            "Eat"
    };

    volatile int totalIterations = 0;

    public static void main(String[] args) {
        int iterations = getIterations(args);

        Main m = new Main();

        int checkSum = 0;
        for (String name : threads) {
            checkSum += name.length();
        }
        m.monitor.setCheckSum(checkSum);
        long start = System.currentTimeMillis();
        for (int t = 0; t < 10000; t++) {
            for (int i = 0; i < threads.length; i++) {
                String name = threads[i];
                Runner thread = m.new Runner(name, iterations, i);
                thread.start();
                if (i == threads.length - 1) //if this is last thread
                    try {
                        thread.join(); //wait until it finishes
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
        }
        System.out.println(String.format("Total time: %s", System.currentTimeMillis() - start));
        System.out.println(String.format("Mean worthless iterations: %s", m.totalIterations / 10000.0));
    }

    private class Runner extends Thread {

        private final int iterations;
        private int index;

        private Runner(String name, int iterations, int index) {
            this.iterations = iterations;
            this.index = index;
           // setPriority(10 - (index << 1 | 1));
            setName(String.valueOf(name));
        }

        @Override
        public void run() {
            int resultIterations = 0;
            for (int i = 0; i < iterations; ) {
                resultIterations++;
                if (SAY_MY_NAME(getName(), index))
                    i++;
            }
            resultIterations -= iterations;
            synchronized (monitor) {
                totalIterations += resultIterations;
            }
        }

        boolean SAY_MY_NAME(String name, int index) {

            synchronized (monitor) {
                if (monitor.checkQueue(index)) {
                    monitor.add(name);
                    if (monitor.iterationIsDone()) {
                        monitor.notifyAll();
                        Thread.yield();
                    } else try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            }
            Thread.yield();
            return false;
        }
    }

    private class Monitor {

        private final Object innerMonitor = new Object();

        volatile int queueIndex = 0;
        volatile String result = "";

        private int stringSum = 0;

        private int checkSum = 0;

        /**
         * Checks if current thread can add its name
         *
         * @param index - index of a thread
         * @return true if it can, else otherwise
         */
        public boolean checkQueue(int index) {
            synchronized (innerMonitor) {
                return queueIndex == index;
            }
        }

        /**
         * Synchronized concatenation of name with result string
         *
         * @param threadName - name of a thread that should be added to the result string
         */
        public void add(String threadName) {
            synchronized (innerMonitor) {
                result += threadName;
                stringSum += threadName.length();
                queueIndex++;
                if (threads.length == queueIndex) {
                    onReady();
                }
            }
        }

        private void onReady() {
            if (stringSum == checkSum) {
                //System.out.println(result);
                result = "";
                queueIndex = 0;
                stringSum = 0;
            } else {
                System.err.println(String.format("The result string has invalid length: %s, expected: %s", stringSum, checkSum));
                System.exit(1);
            }
        }

        /**
         * Sets the result length of iteration result string
         *
         * @param checkSum - size of expected result string
         */
        public void setCheckSum(int checkSum) {
            if (isMainThread())
                this.checkSum = checkSum;
            else throw new RuntimeException("Must be set from main thread");
        }

        private boolean isMainThread() {
            return Thread.currentThread().getId() == 1;
        }

        public boolean iterationIsDone() {
            synchronized (innerMonitor) {
                return queueIndex == 0;
            }
        }
    }

    private static int getIterations(String[] args) {
        int iterations = 5;
        if (args.length > 0) {
            String arg = args[0];
            try {
                iterations = Integer.parseInt(arg);
            } catch (NumberFormatException e) {
                System.out.println(String.format("Enter correct iteration count! Using default value: %s", iterations));
            }
            System.out.println(String.format("Using iteration value: %s", iterations));
        } else
            System.out.println(String.format("Enter an iteration count! Using default value: %s", iterations));
        return iterations;
    }
}
